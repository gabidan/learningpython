# no functions allowed 

a = [0,1,2,3,4,5,6,7,8,9]
a = [0,1,2,3,4,5,6,7,8]



#print (a[0:(int(len(a)/2))])
#print (a[int(len(a)/2):int(len(a)-1)])

a = [] 

#print (a[0:len(a)])l



a = [0] 

#print (a[0:len(a)])
#print (a[0:0])
#print (a[0:1])


a = [0,1,2,3,4,5,6,7,8]

#print (a[0:2])
#print (a[0:3])
#count from 1, index from 0
# my_list[index:count]

#index = (index + count)-1

# merge a and b 
a = [1,2,3]
b = [1,2,3]

c = a + b
c = sorted(c)
print (c)

assert c == [1,1,2,2,3,3]


# merging lists,dictionaries and stuff
# reverse 'a' and store in 'c' 
#print(a)

a.reverse()
c = a
print (c)

assert c == [3,2,1]



# merge a,c 

d = a+c
#assert d == [3,1,2,2,3,1]


# intersection 

e = [1,2,4,5,6,7,3] 

# remove a from e to create f


f = [val for val in e if val not in a]
print (f)

#assert f == [2,4,5,6,7]


g = ["a","b","c"]

#g holds the keys for list a 
# create dictionary 'd'

a.reverse()

d = {}
for i in range (0,len(g)):
    x = i
    d[g[i]]=a[x]
print (d)


assert d == {"a":1,"b":2,"c":3}


h = [[1,2,3],[10,20,30],[100,200,300]] 


# build 'i' and pass the assert below 

i = []
for x in range (0,len(h)):
    d = {}
    i.append(d)
    for j in range (0,len(h[x])):
        v = h[x][j]
        k=g[j]
        d[k]=v
print (i)


assert i == [{"a":1,"b":2,"c":3},{"a":10,"b":20,"c":30} ,{"a":100,"b":200,"c":300} ]

# 'a' holds the keys for values in 'h'
# build 'j'  

j = {}
for i in range (0,len(g)):
    k = g[i]
    j[k] = h[i]
print(j)


assert j =={"a":[1,2,3], "b":[10,20,30], "c":[100,200,300]}


names = ["bobby","tommy","james","bobby","jeffery","charles","harry","james" ] 

#count the number of unique names 

newnames = []
for i in range (0,len(names)):
    x = names[i]
    while x not in newnames:
        newnames.append(x)
print(newnames)

v = len(newnames)
print (v)
# use the variable v to store this number 
assert v == 6 

# list only the names with duplicates and ignore the rest 

for x in set(names):
    names.remove(x)

duplicates = list(set(names))
print (duplicates)

assert set(duplicates) ==set(["bobby","james"])

x = [1,3,4,5,7,9,0] 


a, b = x.index(4), x.index(5)
x[b], x[a] = x[a], x[b]
print (x)

y = x











# swap 4 and 5 

assert y == [1,3,5,4,7,9,0] 

### Now you can use functions again 

z = [1,2,6,5,4] 


