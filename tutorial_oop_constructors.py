
def should_exit(b):
	if b is True: 
		import sys;
		sys.exit() 
	else:
		return 

# Let's take a look at classes 

class AClass():
	pass 

print (AClass)

should_exit(True)  ## as you go through this tutorial , move this line down. The program EXITS at this line.

# AClass is a type just the same way an string, integer and floats are all types
# AClass is what's called a user defined type because you/me created it
# It didn't come with python. 
# Now lets a create a new variable using this new type AClass 


a = AClass() 

# 'a' is of type of AClass and it doesn't really do much.
# Look at the definition at the top - no methods, no attributes. 

print (a)


# on my computer this is what's printed out
#<__main__.AClass instance at 0xb74c7bac>  
# the first part says what type it is - the second bit 0xb74c7bac is a memory location of where our object is stored
# That number will be different on your computer. Let's create another one.

a1 = AClass()

print (a1)

#<__main__.AClass instance at 0xb74c4bcc> 
# The memory locations are almost identical but THEY DO differ. 


# Whenever you create a new object the method __init__ is ALWAYS executed. If you do not define one, python runs it's own, which YOU GUESSED IT, does nothing.  


#Okay, I think we're ready to take look at a construcors.

#Let's create a new __init__ function 


# Our class 'AClass' does jacksh*t. 


class BClass():
	def __init__(self):
		print "Hey, I am the BClass constructor -- -woooohooo " 


b = BClass() 
 
#next thing to notice  is that whenever a function is defined in a class scope the first parameter
# is always 'self'

# Attributes 

# let's create a new variable in a class  

class DClass():
	def __init__(self):
		self.var1 = 0 

d = DClass() 
print (d) 
print d.var1  

# NOw let's add a new method that uses a variable initialised in your constructor __init__(self):
# Look at the class below 

class EClass():
	def __init__(self):
		self.var1=0

	def method0(self):
		print (self.var1) 

	
e = EClass()
e.method0() 

# Notice that method0() has NO PARAMETERS 
# the 'self' parameter is ALWAYS ignore. It's their only for the programmer 
# Not for the user of the class


# Now let's create a class where the user gives the inital values for variables stored in the class
# by passing a parameter to the __init__ method ( also called a constructor) 
# __init__(self,paramA,paramB,paramC .... paramN)

class FClass():
	def __init__(self,v):
		self.var1 = v 

	def print_var(self):
		print(self.var1) 

f = FClass(302020)
f.print_var()



###  Part 2 
# Now when have attributes stored in a class, it may be useful to be able to change them as we wish.
# So let's add a few variables that will update a variable stored in a class.

class GClass():
	def __init__(self):
		self.name = "" 

	def set_name(self,a_name):
		self.name  = a_name 


g = GClass()
print (g.name)
# This prints nothing because self.name="" when the class is created

g.set_name("Jack") 


# no self.name has been  update to Jack 

print (g.name) 


g0 = GClass()
g0.set_name("Jill") 

print (g0.name) 
print (g.name) 
#Notice the g.name and g0.name are set to Jack and Jill respectively 
#Now let's add a method which prints a name 

class NamePrinter():
	def __init__(self):
		self.name = ""

	def set_name(self,a_name):
		self.name = a_name 

	def print_name(self):
		print(self.name) 


np = NamePrinter()
np.set_name("Jimmy") 
np.print_name() 

#### Now that we've done the basics of building classes let's look at a few noddy examples


class MultiNamePrinter():
	def __init__(self):
		self.names=[] 

	def print_names(self):
		print("Printing names....START") 
		for n in self.names:
			print("Name:",n)

		print("Printing names....DONE") 
	def add_name(self,n):
		print(" * adding new name ",n) 
		self.names.append(n)


mnp  = MultiNamePrinter()    
mnp.add_name("Terence") 

mnp.print_names() 

 
mnp.add_name("Polly") 
mnp.print_names()


## Objects/Classes are there to do work for us.

class FileStats():
	def __init__(self,file_handle):
		self.file = file_handle

	def count_lines(self):
		self.file.seek(0) # reset the file pointer to the beginning of the file 
		counter = 0 
		for line in self.file:
			counter = counter + 1 

		return counter
 
	def count_words(self):
		self.file.seek(0)
		counter  = 0 
		for line in self.file:
			line = line.strip() 
			words = [w for w in line.split() if w is not " "]
			counter = counter + len(line.split(" ")) 
		return counter 

fs = FileStats(open("README.md"))

print("Line count;",fs.count_lines())   
print("Word count;",fs.count_words())   


# Can you see that our class is now doing real work ! 

file_names = ["README.md","goog.csv","lesson2.py"] 

for fname in file_names:
	print ("____________________________",fname,"_____________________") 
	f = open(fname) 
	fs = FileStats(f) 
	print("Line count;",fs.count_lines())   
	print("Word count;",fs.count_words())   
