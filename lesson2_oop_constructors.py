# Lesson on constructors 
#import self as self



class C():
	def __init__(self):
		self._name = ""


# 
pdc = C("bob")

assert c.name == "bob" 



class D():
	def __init__(self,name):
		self._name = ""

	def set_name(self,name1):
		self.name = name1

d = D()
d.set_name("bob")

assert d.name == "bob" 


class E():
	def __init__(self):
		return self


e = E() 

g =  e.create_d("bob") 
assert g.name =="bob" 

class G():
	def __init__(self):
		self._number == 10


g = G(10)
assert g.number == 10 


class Counter():
	def __init__(self,counter):
		self.value = 0

	def inc(self, counter1):
		for i in range (0,len(counter1)):
			counter.value = counter +1





counter = Counter(4)

assert counter.value() == 0 

counter.inc() 

assert counter.value() == 1 

counter.inc() 

assert counter.value() == 2 

assert counter.remaining() == 1  

assert counter.has_more() == True 

counter.inc() 

assert counter.value() == 4 

assert counter.has_more == False 

new_counter = Counter(5)

while new_counter.has_more() == True:
	print (counter.value())
	counter.inc() 

assert new_counter.value() == 4 

new_counter.dec() 

assert new_counter.value()  == 3 

new_counter.dec() 

assert new_counter.value() == 2 


class Stack():
	pass 

# create a stack with a maximum of 5 elements 
# use a counter object to keep track of how many elements you have in your stack 
s = Stack(5) 

s.push(1) 
# so when you do push, you should increment your counter object 
assert s.size() == 1 

assert s.is_full() == False 
# use the counter object to see if your stack is full 
s.push(2)
s.push(3)
s.push(4)
s.push(5)

assert s.is_full() == True 

v = s.pop() 
# when pop is called don't forget to decrement your counter object 
assert v == 5 

assert s.is_full()==False 
