Gabbies project files and exercies 

 * data strucures 
 * operating systems 
 * general programming 


# An issue is opened for each subject 

The issue tracker is used to track the various topics in the class 

 * [Operating Systemes - General ](https://bitbucket.org/gabidan/learningpython/issues/1/operating-systems)

# git basic commands - cheat sheet  

## Add a new file to your repo(sitory) 

	git add path/to/file 


## commit changes to files 

	git commit -m " * data structure answers " path/to/file1 path/to/file2 ...

## push files to repository 

	git push origin master 

"origin" is the name of your the online version of your repository. You need to push your changes to the internet for it to be visible to everyone else 


## pull files to from online ( remote) repository to your local git repo 

	git pull origin master

This will pull the changes from your remote repository (referred to by origin) to your local repository referred to by master. 

  
